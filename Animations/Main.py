from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.animation import Animation
from kivy.uix.floatlayout import FloatLayout
from kivy.properties import ObjectProperty

class Root(FloatLayout):
	buttonB = ObjectProperty(None)

	def __init__(self):
		FloatLayout.__init__(self)

		#Button A
		buttonA = Button(text="A", x= 50, y=50, size_hint=(0.1,0.1))
		self.add_widget(buttonA)

		#Sequential animation for movement and scaling
		a1 = Animation(y=250,duration = 2)
		a2 = Animation(x=250, size_hint = (.2,0.05),duration = 2, t="out_elastic")
		a3 = Animation(y=50,duration = 1, t = "out_bounce")
		a4 = Animation(x=50, size_hint = (0.1,0.1), duration = 2)

		aFull = a1 + a2 + a3 + a4
		aFull.repeat=True
		aFull.start(buttonA)

		#Button B
		buttonB = Button(text="B", x = 550, y=350, size_hint=(0.1,0.1), background_color=(0,1,0,1))
		self.add_widget(buttonB)

		#Sequential animation for movement
		b1 = Animation(y=480,duration = 2)
		b2 = Animation(x=50,duration = 2, t ="in_circ")
		b3 = Animation(y=350,duration = 2)
		b4 = Animation(x=550, duration = 2, t ="out_quint")

		bMove = b1 + b2 + b3 + b4
		bMove.repeat=True

		#Sequential animation for color
		bs1 = Animation(background_color=(1,0,1,1), duration = 1)
		bs2 = Animation(background_color=(0,1,0,1), duration = 1)

		bScale = bs1 + bs2
		bScale.repeat=True

		#Parallel animation for both movement and color
		bFull = bMove & bScale
		bFull.start(buttonB)

class MainApp(App):
	def build(self):
		return Root()

MainApp().run()
