from kivy.app import App
from kivy.uix.widget import Widget
from kivy.graphics import Color, Ellipse
from kivy.properties import ObjectProperty, NumericProperty
import random as rand

class Root(Widget):
	#ObjectProperty is used for referencing widgets created in the .kv file
	clickLabel = ObjectProperty(None)
	clickCount = 0

	def CreateBall(self, touch):
		with self.canvas.before:
			#Create a randomly colored and sized ball on touch position
			Color(rand.random(), rand.random(), rand.random())
			radius = rand.uniform(20, 80)
			Ellipse(pos=(touch.x - radius / 2, touch.y - radius / 2), size=(radius, radius))

			#Update click counter
			self.clickCount += 1
			self.clickLabel.text = str(self.clickCount)

	#Automatic widget touch callbacks
	def on_touch_down(self, touch):
		self.CreateBall(touch)

	def on_touch_move(self, touch):
		self.CreateBall(touch)

class MainApp(App):
	def build(self):
		return Root()

MainApp().run()
