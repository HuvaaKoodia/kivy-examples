# Kivy examples

A few example apps using the Kivy GUI library for python.

## Includes

- Basic and custom button examples
- Sequential and parallel animation examples
- Kv language usage examples
- A widget input example
- An empty app template

## Requirements

- [Python 3](https://www.python.org)
- [Kivy](https://kivy.org)

## Usage

Run
```
python3 Main.py
```
in any of the example folders.


## Licence

MIT Licence