from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.button import Button
from kivy.graphics import Rectangle, Color
import random as rand

#Subclasses for graphical modification
class GraphicsButton1(Button):
	pass#Some tweaks in the .kv file

class GraphicsButton2(Button):
	rectangles = []

	def __init__(self, **kwargs):
		Button.__init__(self, **kwargs)
		print(kwargs)
		self.background_color=(0,0,0,0)
		#Listen to size or pos changes
		self.bind(size=self.UpdateCanvas, pos=self.UpdateCanvas)

	def OnPress(self, b):
		self.text="Colors!"
		with self.canvas.before:
			#Rectangle stats
			size = rand.random()*0.5
			posX = rand.random()

			#Add rectangle
			Color(rand.random(), rand.random(), rand.random())
			rect = Rectangle(pos=(self.pos[0]+posX*self.width, self.pos[1]), size=(size*self.width, self.height))

			#Save stats for canvas update
			self.rectangles.append((size,posX,rect))

	def UpdateCanvas(self, instance, value):
		for r in self.rectangles:
			size = r[0]
			posX = r[1]
			rect = r[2]
			rect.pos = (self.pos[0]+posX*self.width, self.pos[1])
			rect.size = (size*self.width, self.height)

class MainApp(App):
	def build(self):
		root = GridLayout(rows=2, cols=2)

		#Button 1
		b1 = Button(text="Press here!", font_size = 40)
		b1.bind(on_press = lambda x: print(f"{x.text} callback from a lambda!"))
		root.add_widget(b1)

		#Button 2
		def OnButtonPress(button):
			print(f"{button.text} callback from a method!")

		b2 = Button(text="Or here!", font_size = 40)
		b2.bind(on_press = OnButtonPress)
		root.add_widget(b2)

		#Graphics button 1
		gb1 = GraphicsButton1(text="Look at me!", font_size = 40)

		def onPress(self):
			gb1.text=f"Don't press me!"
			gb1.background_color = (1,0,0,1)
			gb1.color = (1,1,1,1)

		gb1.bind(on_press = onPress)
		root.add_widget(gb1)

		#Graphics button 2
		gb2 = GraphicsButton2(text="Colors?", font_size = 40)
		gb2.bind(on_press = gb2.OnPress)
		root.add_widget(gb2)

		return root

MainApp().run()
