from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ObjectProperty
import random

#A custom root is needed so that object properties work.
class Root(BoxLayout):
	#ObjectProperty is used for referencing widgets created in the .kv file
	label = ObjectProperty(None)

	#Variables, callbacks, and related logic is defined in code
	chars = []

	def addChar(self, button):
		self.label.text += button.text
		self.chars.append(button.text)

	def mixChars(self):
		random.shuffle(self.chars)

		self.label.text = ""
		for c in self.chars:
			self.label.text += c

	def clearChars(self):
		self.label.text = ""
		self.chars = []

class MainApp(App):
	pass#Root instance is create in the .kv file

MainApp().run()
