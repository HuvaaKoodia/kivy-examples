from kivy.app import App
from kivy.uix.widget import Widget

class MainApp(App):
	def build(self):
		#Widgets can be added here in the build method
		#or in the .kv file using the kv language
		root = Widget()
		return root

MainApp().run()
